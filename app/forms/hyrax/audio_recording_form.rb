# Generated via
#  `rails generate hyrax:work AudioRecording`
module Hyrax
  # Generated form for AudioRecording
  class AudioRecordingForm < Hyrax::Forms::WorkForm
    self.model_class = ::AudioRecording
    self.terms += [:resource_type, :contact_email]
  end
end
