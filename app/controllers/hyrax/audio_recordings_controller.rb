# Generated via
#  `rails generate hyrax:work AudioRecording`
module Hyrax
  # Generated controller for AudioRecording
  class AudioRecordingsController < ApplicationController
    # Adds Hyrax behaviors to the controller.
    include Hyrax::WorksControllerBehavior
    include Hyrax::BreadcrumbsForWorks
    self.curation_concern_type = ::AudioRecording

    # Use this line if you want to use a custom presenter
    self.show_presenter = Hyrax::AudioRecordingPresenter
  end
end
