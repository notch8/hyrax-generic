require 'rails_helper'
include Warden::Test::Helpers

RSpec.feature "Create Collections", type: :feature do
  context 'Going to a website' do
    let(:user_attributes) do
      { email: 'test@example.com' }
    end
    let(:user) do
      User.new(user_attributes) { |u| u.save(validate: false) }
    end

    let(:collection_id) { Hyrax::CollectionType.find_or_create_default_collection_type.gid }
    let(:permission_template) { Hyrax::PermissionTemplate.find_or_create_by!(source_id: collection_id) }
    let(:workflow) { Sipity::Workflow.create!(active: true, name: 'test-workflow', permission_template: permission_template) }

    before do
      # Create a single action that can be taken
      Sipity::WorkflowAction.create!(name: 'submit', workflow: workflow)

      # Grant the user access to deposit into the collection.
      Hyrax::PermissionTemplateAccess.create!(
        permission_template_id: permission_template.id,
        agent_type: 'user',
        agent_id: user.user_key,
        access: 'deposit'
      )

      login_as user
    end

    Steps 'Creating a new collection for works' do
      Given 'I have logged in and go to my dashboard' do
      end
      When 'I visit and view the Collections section' do
        visit '/dashboard/my/collections?locale=en'
        expect(page).to have_content('Title', 'Type')
        expect(page).to have_css('input#search-field-header')
      end
      Then 'I can create a new collection' do
        expect(page).to have_content('New Collection')
        click_on 'New Collection'
        expect(page).to have_content('New User Collection')
      end
      And 'I cannot create a collection without a title' do
        click_on 'Save'
        expect(page).to have_no_content('Collection was successfully created.')
      end
      Then 'I fill in the collection information' do
        fill_in 'Title', with: 'Thomas Hardy Complete Works'
        fill_in 'Abstract or Summary', with: 'Text description.'
      end
      And 'I can fill in additional optional information' do
        click_on 'Additional fields'
        fill_in 'Creator', with: 'Hardy, Thomas'
        fill_in 'Keyword', with: 'Literature'
        fill_in 'Publisher', with: 'Osgood, McIlvaine, & Co.'
        within ('#collection_resource_type') do
          select 'Book'
        end
      end
      Then 'I save the collection' do
        click_on 'Save'
        expect(page).to have_content('Collection was successfully created.')
      end
      When 'I have saved the collection, I can edit the collection information' do
        expect(page).to have_content('Edit User Collection')
        expect(page).to have_button('Save changes')
      end
      And 'I see the various tabs for editing and adding more inforamtion' do
        expect(page).to have_content('Branding')
        expect(page).to have_content('Sharing')
        expect(page).to have_content('Discovery')
      end
      Then 'I can update my discovery preferences' do
        click_on 'Discovery'
        find('input#visibility_open').click
        click_on 'Save changes'
        expect(page).to have_content('Collection was successfully updated.')
      end
      When 'I visit my collections page, I see the collection I submitted' do
        visit '/dashboard/my/collections?locale=en'
        expect(page).to have_content('Title', 'Thomas Hardy Complete Works')
        expect(page).to have_content('Visibility', 'Public')
      end
      When 'I visit the homepage, I see the collection I submitted' do
        visit '/'
        expect(page).to have_content('Thomas Hardy Complete Works')
      end
      And 'I can search for the collection' do
        fill_in 'Search Hyrax', with: 'Thomas Hardy'
        find('button#search-submit-header').click
        expect(page).to have_content('Limit your search')
        expect(page).to have_content('Filtering by:', 'Thomas Hardy')
        fill_in 'Search Hyrax', with: 'BBBBBB'
        find('button#search-submit-header').click
        expect(page).to have_content('No results found for your search')
      end
    end
  end
end
