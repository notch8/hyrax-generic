require 'rails_helper'

RSpec.feature "SignUpPages", type: :feature, js: true do
  context 'Going to website' do
    Steps 'Creating a new user' do
      Given 'I am on the sign up page' do
        visit '/users/sign_up?locale=en'
      end
      Then 'I can see a sign up title above the form' do
        expect(page).to have_content("Sign up")
      end
      When 'The Password fields do not match' do
        fill_in 'Email', with: 'example@example.com'
        fill_in 'Password', with: 'Password1'
        fill_in 'Password confirmation', with:'Password2'
        click_button 'Sign up'
        expect(page).to have_content("Password confirmation doesn't match Password")
      end
      When 'I fill in the sign up form and submit' do
        fill_in 'Email', with: 'example@example.com'
        fill_in 'Password', with: 'Password1'
        fill_in 'Password confirmation', with:'Password1'
        click_button 'Sign up'
      end
      Then 'I am directed to the dashboard ' do
        expect(page).to have_current_path '/dashboard?locale=en'
      end
    end
  end
end
