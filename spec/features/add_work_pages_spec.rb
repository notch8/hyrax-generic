require 'rails_helper'
include Warden::Test::Helpers

RSpec.feature "Add a Work Pages", type: :feature do
  context 'Going to website' do
    let(:user_attributes) do
      { email: 'test@example.com' }
    end
    let(:user) do
      User.new(user_attributes) { |u| u.save(validate: false) }
    end

    let(:test_image_file_path) {File.expand_path( "#{Hyrax::Engine.root}/spec/fixtures/image.jp2", __FILE__)}
    let(:admin_set_id) { AdminSet.find_or_create_default_admin_set_id }
    let(:permission_template) { Hyrax::PermissionTemplate.find_or_create_by!(source_id: admin_set_id) }
    let(:workflow) { Sipity::Workflow.create!(active: true, name: 'test-workflow', permission_template: permission_template) }

    before do
      # Create a single action that can be taken
      Sipity::WorkflowAction.create!(name: 'submit', workflow: workflow)

      # Grant the user access to deposit into the admin set.
      Hyrax::PermissionTemplateAccess.create!(
        permission_template_id: permission_template.id,
        agent_type: 'user',
        agent_id: user.user_key,
        access: 'deposit'
      )
      login_as user
    end

    Steps 'Adding a new work' do
      Given 'A workflow and permission template have been set, and I am logged in' do
      end
      When 'I visit my works page' do
        visit "/dashboard/my/works?locale=en"
      end
      Then 'I see the information of previously added works' do
        expect(page).to have_content('Title', 'Date Added')
      end
      When 'I click on "Add new work" and select a file type' do
        click_on 'Add new work'
        within ('#worktypes-to-create') do
          expect(page).to have_content('Select type of work')
          find('input[value="Work"]').click
          click_on 'Create work'
          expect(page).to have_current_path('/dashboard/my/works?payload_concern=Work')
        end
      end
      When 'I fill out info for the work I want to upload' do
        visit ('/concern/works/new?locale=en')
        expect(page).to have_content("Add New Work")
        expect(page).to have_content('Title')
        fill_in 'Title', with: 'Worf Title'
        fill_in 'Creator', with: 'Smiff, John'
        fill_in 'Keyword', with: 'Content'
        within ('#work_rights_statement') do
          find('option[value="http://rightsstatements.org/vocab/NoC-NC/1.0/"]').click
        end
      end
      And 'I enter additional optional information' do
        click_on 'Additional fields'
        expect(page).to have_content('Contributor', 'License')
      end
      And 'I select a visibility setting' do
        within ('.set-access-controls') do
          find('input[value="open"]').click
        end
      end
      And 'I upload the work file' do
        click_on 'Files'
        expect(page).to have_content('You can add one or more files to associate with this work.')

        within ('span#addfiles') do
          attach_file('files[]', "#{test_image_file_path}", visible: false)
        end
      end
      When "I haven't entered all the necessary information, the save button is not clickable" do
        expect(page).to have_button('Save', visible: false)
      end
      And 'I can agree to the Deposit Agreement and the save button becomes visible' do
        check('agreement')
        expect(page).to have_button('Save', visible: true)
        click_on 'Save'
      end
      When 'I am redirected to the specific work page, I see that my work uploaded successfully' do
        expect(page).to have_content('Work', 'Items')
        expect(page).to have_content('Your files are being processed by Hyrax in the background. The metadata and access controls you specified are being applied. You may need to refresh this page to see these updates.')
      end
      And 'I can share or edit my work' do
        visit '/dashboard/my/works?locale=en'
        within (page.first(:css, '.media-heading')) do
          page.find('a:first-child').click
      end
        expect(page).to have_css('.resp-sharing-button--facebook')
        expect(page).to have_content('Items')
        within ('.show-actions') do
          click_on 'Edit'
      end
        expect(page).to have_content('Edit Work')
      end
    end
  end
end
