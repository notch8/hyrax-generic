require 'rails_helper'

RSpec.feature "TransfersPages", type: :feature do
  before :each do
    User.create(email: 'example@example.com', password:'Password1')
  end
  context 'Going to website' do
    Steps 'Going to transfers page' do
      Given 'I have successfully logged in and went into transfers page' do
      end
      When 'I am successfully logged in and go to the transfers page' do
        visit '/users/sign_in?locale=en'
        expect(page).to have_content("Log in")
        fill_in 'Email', with: 'example@example.com'
        fill_in 'Password', with: 'Password1'
        click_button 'Log in'
      end
      When 'I am successfully logged in and go to the transfers page', js:true do
        expect(page).to have_current_path '/dashboard?locale=en'
        find("span", :text => "Your activity", visible: false).first(:xpath,".//..").click
        find("span", :text => "Transfers", visible: false).first(:xpath,".//..").click
        expect(page).to have_content("Transfers")
      end
      Then 'I expect to see a Transfers Sent section' do
        expect(page). to have_content("Transfers Sent")
      end
      When 'I did not transfer any works' do
        expect(page). to have_content("You haven't transferred any work")
      end
      Then 'I expect to see a Transfers Received section' do
        expect(page). to have_content("Transfers Received")
      end
      When 'I did not receive any work transfer requests' do
        expect(page). to have_content("You haven't received any work transfer request")
      end
    end
  end
end
