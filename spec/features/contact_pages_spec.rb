require 'rails_helper'

RSpec.feature "ContactPages", type: :feature do
  before :each do
    User.create(email: 'example@example.com', password:'Password1')
  end
  context 'Going to website' do
    Steps 'Going to contact page' do
      Given 'I am on the contact page' do
        visit '/contact?locale=en'
      end
      Then 'I can see the Contact Form' do
        expect(page). to have_content("Issue Type")
        expect(page). to have_content("Your Name")
        expect(page). to have_content("Your Email")
        expect(page). to have_content("Subject")
        expect(page). to have_content("Message")
        expect(page). to have_content("Please use the contact form to submit inquiries about this system; to report a problem you are experiencing with the system; to request assistance using the system; or to provide general feedback. See the Help page for additional information about this system.")
      end
      Then 'I can pick the selection from Issue Type dropdown' do
        select "Depositing content", :from => "contact_form_category"
        select "Making changes to my content", :from => "contact_form_category"
        select "Browsing and searching", :from => "contact_form_category"
        select "Reporting a problem", :from => "contact_form_category"
        select "General inquiry or request", :from => "contact_form_category"
      end
      Then 'I can fill in the Contact Form' do
        fill_in 'Your Name', with: 'Jane Doe'
        fill_in 'Your Email', with: 'example@example.com'
        fill_in 'Subject', with: 'Hello'
        fill_in 'Message', with: 'Hello'
        click_button 'Send'
      end
      Then 'I can see the success message that the contact form was created' do
        expect(page). to have_content("Thank you for your message!")
      end
      When 'I do not fill in the information in the contact form' do
        fill_in 'Your Name', with: ''
        fill_in 'Your Email', with: ''
        fill_in 'Subject', with: ''
        fill_in 'Message', with: ''
        click_button 'Send'
        expect(page). to have_no_content("Thank you for your message!")
      end
    end
  end
end
