require 'rails_helper'

RSpec.feature "NotificationPages", type: :feature do
  before :each do
    User.create(email: 'example@example.com', password:'Password1')
  end
  context 'Going to website' do
    Steps 'Viewing content' do
      Given 'I am on the notifications page after I successfully log in' do
      end
      When 'I am successfully logged in and go to the notifcations page' do
        visit '/users/sign_in?locale=en'
        expect(page).to have_content("Log in")
        fill_in 'Email', with: 'example@example.com'
        fill_in 'Password', with: 'Password1'
        click_button 'Log in'
      end
      When 'I am successfully logged in and I see the notifications page', js:true do
        expect(page).to have_current_path '/dashboard?locale=en'
        find("span", :text => "Your activity", visible: false).first(:xpath,".//..").click
        find("span", :text => "Notifications", visible: false).first(:xpath,".//..").click
        expect(page).to have_content("Notifications")
      end
      Then 'I am able to see the Delete All button' do
        expect(page).to have_selector(:css, "a.btn-danger" )
      end
      Then 'If there are no notifications, then I see No notifications found' do
        expect(page). to have_content("No notifications found")
      end
      When 'I click on the Delete button' do
        click_link 'Delete All'
        expect(page). to have_content("Notifications have been deleted")
      end
    end
  end
end
