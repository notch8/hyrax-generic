require 'rails_helper'

RSpec.feature "ProfilePages", js:true, type: :feature do
  before :each do
    User.create(email: 'example@example.com', password:'Password1')
  end
  context 'Going to website' do
    Steps 'Going to profile page' do
      Given 'I have successfully logged in and went into profile page' do
      end
      When 'I am successfully logged in and go to the profile page' do
        visit '/users/sign_in?locale=en'
        expect(page).to have_content("Log in")
        fill_in 'Email', with: 'example@example.com'
        fill_in 'Password', with: 'Password1'
        click_button 'Log in'
      end
      When 'I am successfully logged in and go to the profile page' do
        expect(page).to have_current_path '/dashboard?locale=en'
        find("span", :text => "Your activity", visible: false).first(:xpath,".//..").click
        find("span", :text => "Profile", visible: false).first(:xpath,".//..").click
        expect(page).to have_content("Profile")
        expect(page).to have_content("Joined on")
      end
      Then 'I can see the collections page' do
         expect(page).to have_link("Collections created", :href=>"/catalog?f%5Bgeneric_type_sim%5D%5B%5D=Collection&locale=en&q=%22example%40example.com%22&search_field=depositor")
      end
      Then 'I can see the works created page' do
        expect(page).to have_link("Works created", :href=>"/catalog?f%5Bgeneric_type_sim%5D%5B%5D=Work&locale=en&q=%22example%40example.com%22&search_field=depositor")
      end
      Then 'I can see one primary button' do
        expect(page).to have_selector(:css, "a.btn-primary" )
      end
      Then 'I see my email crendetial' do
        expect(page).to have_text('Email')
      end
      When 'I click the Edit Profile button' do
        click_link 'Edit Profile'
        expect(page).to have_current_path '/dashboard/profiles/example@example-dot-com/edit?locale=en'
        expect(page).to have_content("Edit Profile")
      end
      When 'I can check delete picture' do
        check('Delete picture')
      end
      When 'I enter my new profile credentials' do
        fill_in 'ORCID Profile', with: '1234-1234-1234-1234'
        fill_in 'Twitter Handle', with: 'twitterexample'
        fill_in 'Facebook Handle', with: 'facebookexample'
        fill_in 'Google+ Handle', with: 'googleexmaple'
        click_button 'Save Profile'
      end
      When 'I am redirected to my dashboard' do
        expect(page).to have_current_path '/dashboard/profiles/example@example-dot-com?locale=en'
      end
      Then 'I can see the new profile information' do
        expect(page).to have_text('ORCID Profile')
        expect(page).to have_text('Facebook Handle')
        expect(page).to have_text('Twitter Handle')
        expect(page).to have_text('Google+ Handle')
      end
    end
  end
end
