require 'rails_helper'

RSpec.feature "LogInPages", type: :feature do
  before :each do
    User.create(email: 'example@example.com', password: 'Password1')
  end
  context 'Going to website' do
    Steps 'Logging in as a User' do
      Given 'I go click login' do
        visit '/users/sign_in?locale=en'
        expect(page).to have_content("Log in")
      end
      When 'I do not fill in the email and password information in the login page' do
        fill_in 'Email', with: ''
        fill_in 'Password', with: ''
        click_button 'Log in'
        expect(page).to have_content("Invalid Email or password.")
      end
      When 'I do not fill in the email only' do
        fill_in 'Email', with: ''
        fill_in 'Password', with: 'Password1'
        click_button 'Log in'
        expect(page).to have_content("Invalid Email or password.")
      end
      When 'I do not fill in the password only' do
        fill_in 'Email', with: 'example@example.com'
        fill_in 'Password', with: ''
        click_button 'Log in'
        expect(page).to have_content("Invalid Email or password.")
      end
      When 'I input incorrect Email information' do
        fill_in 'Email', with: 'example@incorrect.com'
        fill_in 'Password', with: 'Password1'
        click_button 'Log in'
        expect(page).to have_content("Invalid Email or password.")
      end
      When 'I input incorrect password information' do
        fill_in 'Email', with: 'example@example.com'
        fill_in 'Password', with: 'Password1000'
        click_button 'Log in'
        expect(page).to have_content("Invalid Email or password.")
      end
      When 'I enter my login credentials' do
        fill_in 'Email', with: 'example@example.com'
        fill_in 'Password', with: 'Password1'
        click_button 'Log in'
      end
      Then 'I am successfully logged in' do
        expect(page).to have_current_path '/dashboard?locale=en'
      end
    end
  end
end
