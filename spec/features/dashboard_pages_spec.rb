require 'rails_helper'

RSpec.feature "DashboardPages", type: :feature do
  before :each do
    User.create(email: 'example@example.com', password:'Password1')
  end
  context 'Going to website' do
    Steps 'Viewing content' do
      Given 'I am on the dashboard page after I successfully log in' do
      end
      When 'I am successfully logged in and go to the dashboard page' do
        visit '/users/sign_in?locale=en'
        expect(page).to have_content("Log in")
        fill_in 'Email', with: 'example@example.com'
        fill_in 'Password', with: 'Password1'
        click_button 'Log in'
        visit '/dashboard?locale=en'
        expect(page).to have_content("My Dashboard")
      end
      Then 'I see the User Activity section' do
        expect(page).to have_content("User Activity")
      end
      When 'I do not have user activity' do
        expect(page).to have_content("User has no recent activity")
      end
      Then 'I see the User Notifications section' do
        expect(page).to have_content("User Notifications")
      end
      When 'I do not have user notifications' do
        expect(page).to have_content("User has no notifications")
      end
      Then 'I see the Manage Proxies section' do
        expect(page).to have_content("Manage Proxies")
        expect(page).to have_content("Authorize Proxies")
        expect(page).to have_content("Current Proxies")
      end
      Then 'I see the Transfer of Ownership section' do
        expect(page).to have_content("Transfers of Ownership")
        expect(page).to have_content("Transfers Sent")
        expect(page).to have_content("Transfers Received")
        expect(page).to have_link("Select works to transfer")
      end
      When 'I do not have any transferred works' do
        expect(page).to have_content("You haven't transferred any work")
      end
      When 'I do not have any transfers received' do
        expect(page).to have_content("You haven't received any work transfer requests")
      end
      Then 'I can select works to tranfer' do
        click_link("Select works to transfer")
        expect(page).to have_current_path("/dashboard/my/works?locale=en")
      end
    end
  end
end
