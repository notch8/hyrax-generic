require 'rails_helper'

RSpec.feature 'HomePages', type: :feature do

  context 'Going to website' do
    Steps 'Viewing content' do
      Given 'I am on the homepage' do
        visit "/"
      end
      Then 'There is content on the homepage' do
        expect(page).to have_content("Featured Works")
        expect(page).to have_content("Explore Collections")
      end
      Then 'I can see one primary button' do
        expect(page).to have_selector(:css, "a.btn-primary" )
      end
      Then 'I can see one feature button' do
        expect(page).to have_selector(:css, "a#recentTab" )
      end
      Then 'I can see link to Terms of Use' do
        expect(page).to have_link("Terms of Use", :href=>"/terms?locale=en")
      end
      Then 'I can pick the selection from Issue Type dropdown' do
        select "Deutsch", :from => "nav-item dropdown open"
        select "English", :from => "nav-item dropdown open"
        select "Español", :from => "nav-item dropdown open"
        select "Français", :from => "nav-item dropdown open"
        select "Italiano", :from => "nav-item dropdown open"
        select "Português do Brasil", :from => "nav-item dropdown open"
        select "中文", :from => "nav-item dropdown open"
      end
    end
  end
end
