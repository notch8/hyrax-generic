# Docker development setup

1) Install Docker.app 

2) gem install stack_car

``` bash
gem install stack_car
```

3) We recommend committing .env to your repo with good defaults. .env.development, .env.production etc can be used for local overrides and should not be in the repo.

4) Download the code

``` bash
$ git clone https://gitlab.com/notch8/hyrax-generic.git
```

5) Start the app

``` bash
$ cd hyrax-generic
$ sc pull
$ sc up
```

6) Set up initial data

``` bash
$ sc be rails db:migrate
$ sc be rails hyrax:default_collection_types:create hyrax:default_admin_set:create
$ sc be rails generate hyrax:work Work
```
